const htmlStandards = require('reshape-standard')
const cssStandards = require('spike-css-standards')
const jsStandards = require('spike-js-standards')
const pageId = require('spike-page-id')
const Contentful = require('spike-contentful')
const locals = {}

module.exports = {
  devtool: 'source-map',
  matchers: {
    html: '*(**/)*.sgr',
    css: '*(**/)*.sss'
  },
  ignore: ['**/layout.sgr', '**/_*', '**/.*', 'readme.md', 'yarn.lock'],
  reshape: htmlStandards({
    locals: (ctx) => locals
  }),
  postcss: cssStandards(),
  babel: jsStandards(),
  plugins: [
    new Contentful({
      addDataTo: locals,
      accessToken: '9a14c1d26fa975cefd86f3cd49a79a654b05d5732ed654ce57fd70753acd0e43',
      spaceId: '6n4dchzgpcdt',
      contentTypes: [
        {
          name: 'articles',
          id: 'article',
          filters: {
            limit: 5
          }
        }
      ],
      json: 'data.json'
    })
  ]
}